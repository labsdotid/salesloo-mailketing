<?php

namespace Salesloo_Mailketing;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Coupon
 */
class Setting
{

    /**
     * Instance.
     *
     * Holds the coupons instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Register Menu
     * 
     * register system info menu
     */
    public function register_menu($submenu)
    {
        $submenu[] = [
            'page_title' => __('Mailketing Integration', 'salesloo'),
            'menu_title' => __('Mailketing Integration', 'salesloo'),
            'capability' => 'manage_options',
            'slug'       => 'salesloo-mailketing',
            'callback'   => [$this, 'page'],
            'position'   => 99
        ];

        return $submenu;
    }

    public function page()
    {
        $saved = '';

        if (isset($_POST['save']) && isset($_POST['__nonce'])) :
            if (check_admin_referer('salesloo-mailketing', '__nonce') && wp_verify_nonce($_POST['__nonce'], 'salesloo-mailketing')) :

                unset($_POST['save']);
                unset($_POST['__nonce']);
                unset($_POST['_wp_http_referer']);

                foreach ($_POST as $key => $value) {
                    \update_option($key, $value);
                }

                \flush_rewrite_rules();

                $saved = '<div id="message" class="updated inline"><p><strong>' . __('Your settings have been saved.', 'salesloo') . '</strong></p></div>';
            endif;
        endif;

        echo '<div class="wrap">';
        echo '<h2>' . __('Mailketing Integration Settings', 'salesloo') . '</h2>';
        echo $saved;
        echo '<form action="" method="post" enctype="multipart/form-data" style="margin-top:30px">';

        \salesloo_field_text([
            'label'       => __('Mailketing Api Token', 'salesloo'),
            'name'        => 'mailketing_api_token',
            'description' => '',
            'value'       => get_option('mailketing_api_token')
        ]);

        \salesloo_field_submit();

        wp_nonce_field('salesloo-mailketing', '__nonce');
        echo '</form>';
        echo '</div>';
    }

    /**
     * product setting
     *
     * @param  mixed $product
     * @return void
     */
    public function product_setting($product)
    {
        if (empty(get_option('mailketing_api_token'))) return;

        $connect = new Connect();
        $get_lists = $connect->get_lists();



        $options = [];

        if (isset($get_lists['status']) && $get_lists['status'] == 'success') {

            foreach ($get_lists['lists'] as $list) {
                $options[$list['list_id']] = $list['list_name'];
            }
        } else {
            \salesloo_field_notice([
                'type' => 'error',
                'message' => isset($get_lists['response']) ? $get_lists['response'] : 'Error getting list from mailketing api',
                'conditional' => [
                    [
                        'field'   => 'autoresponder_service_provider',
                        'value'   => 'mailketing',
                        'compare' => '='
                    ]
                ],
            ]);
        }

        \salesloo_field_select([
            'label'    => __('On Place Order List', 'salesloo'),
            'name'     => 'mailketing_list_on_place_order_event',
            'required' => false,
            'options' => $options,
            'conditional' => [
                [
                    'field'   => 'autoresponder_service_provider',
                    'value'   => 'mailketing',
                    'compare' => '='
                ]
            ],
            'value' => salesloo_get_product_meta($product->ID, 'mailketing_list_on_place_order_event', true),
            'description' => __('This list will be excetuted on place order event', 'salesloo-mailketing')
        ]);

        \salesloo_field_select([
            'label'    => __('On Completed Invoice List', 'salesloo'),
            'name'     => 'mailketing_list_on_invoice_completed_event',
            'required' => false,
            'options' => $options,
            'conditional' => [
                [
                    'field'   => 'autoresponder_service_provider',
                    'value'   => 'mailketing',
                    'compare' => '='
                ]
            ],
            'description' => __('This list will be excetuted on invoice completed event', 'salesloo-mailketing'),
            'value' => salesloo_get_product_meta($product->ID, 'mailketing_list_on_invoice_completed_event', true),
        ]);
    }

    /**
     * add_service_provider
     *
     * @param  mixed $providers
     * @return void
     */
    public function add_service_provider($providers)
    {
        if (empty(get_option('mailketing_api_token'))) return $providers;

        $providers['mailketing'] = __('Mailketing', 'salesloo-mailketing');

        return $providers;
    }



    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        add_filter('salesloo/admin/submenu', [$this, 'register_menu'], 1, 10);
        add_action('salesloo/admin/product/edit/autoresponder/after', [$this, 'product_setting']);
        add_filter('salesloo/admin/product/edit/autoresponder/service_provider', [$this, 'add_service_provider']);
    }
}
