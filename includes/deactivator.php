<?php

namespace Salesloo_Mailketing;

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Salesloo_Mailketing
 * @subpackage Salesloo_Mailketing/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Salesloo_Mailketing
 * @subpackage Salesloo_Mailketing/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Deactivator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate()
	{
	}
}
