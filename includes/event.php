<?php

namespace Salesloo_Mailketing;

/**
 * Hooked salesloo cron event
 *
 * @since      1.0.0
 * @package    Salesloo_Mailketing
 * @subpackage Salesloo_Mailketing/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Event
{

    /**
     * Instance.
     *
     * Holds the coupons instance.
     *
     * @since 1.0.0
     * @access public
     */
    public static $instance = null;

    /**
     * Init.
     *
     * @since 1.0.0
     */
    public static function init()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * construction
     */
    public function __construct()
    {
        add_action('salesloo/event/place_order', [$this, 'execute_on_place_order_event']);
        add_action('salesloo/event/invoice_completed', [$this, 'execute_on_invoice_completed_event']);
    }

    /**
     * execute_on_place_order_event
     *
     * @param  mixed $args
     * @return void
     */
    public function execute_on_place_order_event($args)
    {
        $invoice_id = isset($args['invoice_id']) ? intval($args['invoice_id']) : 0;
        $invoice = salesloo_get_invoice($invoice_id);

        if (!$invoice) return;

        $orders = $invoice->orders();

        $connect = new Connect();

        foreach ($orders as $order) {
            $product = salesloo_get_product($order->product_id);
            $autoresponder_service_provider = $product->meta('autoresponder_service_provider');

            if ($autoresponder_service_provider == 'mailketing') {
                $list_id = $product->meta('mailketing_list_on_place_order_event');

                $customer = get_userdata($order->user_id);

                $param = [
                    'email'      => $customer->user_email,
                    'first_name' => $customer->first_name,
                    'last_name'  => $customer->last_name,
                    'mobile'     => get_user_meta($customer->ID, 'phone', true)
                ];

                $connect->subscribe($list_id, $param);
            }
        }
    }

    /**
     * execute_on_invoice_completed_event
     *
     * @param  mixed $args
     * @return void
     */
    public function execute_on_invoice_completed_event($args)
    {
        $invoice_id = isset($args['invoice_id']) ? intval($args['invoice_id']) : 0;
        $invoice = salesloo_get_invoice($invoice_id);

        if (!$invoice) return;

        $orders = $invoice->orders();

        $connect = new Connect();

        foreach ($orders as $order) {
            $product = salesloo_get_product($order->product_id);
            $autoresponder_service_provider = $product->meta('autoresponder_service_provider');

            if ($autoresponder_service_provider == 'mailketing') {
                $list_id = $product->meta('mailketing_list_on_invoice_completed_event');

                $customer = get_userdata($order->user_id);

                $param = [
                    'email'      => $customer->user_email,
                    'first_name' => $customer->first_name,
                    'last_name'  => $customer->last_name,
                    'mobile'     => get_user_meta($customer->ID, 'phone', true)
                ];

                $connect->subscribe($list_id, $param);
            }
        }
    }
}
