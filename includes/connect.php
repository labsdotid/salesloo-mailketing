<?php

namespace Salesloo_Mailketing;

/**
 * Define connect classes with mailketing api
 *
 * @since      1.0.0
 * @package    Salesloo_Mailketing
 * @subpackage Salesloo_Mailketing/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Connect
{

    private $_api_token;

    /**
     * construction
     */
    public function __construct()
    {
        $this->_api_token = get_option('mailketing_api_token');
    }

    public function get_lists()
    {
        $response = wp_remote_post(
            'https://app.mailketing.id/api/v1/viewlist',
            array(
                'method'      => 'POST',
                'timeout'     => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking'    => true,
                'headers'     => array(),
                'body'        => array(
                    'api_token' => $this->_api_token,
                ),
                'cookies'     => array()
            )
        );

        if (is_wp_error($response)) {
            $output = [
                'status' => 'error',
                'message' => $response->get_error_messages()
            ];
        } else {
            $output = json_decode(wp_remote_retrieve_body($response), true);
            $code = (int) wp_remote_retrieve_response_code($response);
        }

        return $output;
    }

    public function subscribe($list_id, $param)
    {
        $body = [
            'api_token' => $this->_api_token,
            'list_id' => $list_id
        ];

        $body = wp_parse_args($param, $body);
        $response = wp_remote_post(
            'https://app.mailketing.co.id/api/v1/addsubtolist',
            array(
                'method'      => 'POST',
                'timeout'     => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking'    => true,
                'headers'     => array(),
                'body'        => $body,
                'cookies'     => array()
            )
        );

        if (is_wp_error($response)) {
            $output = [
                'status' => 'error',
                'message' => $response->get_error_messages()
            ];
        } else {
            $output = json_decode(wp_remote_retrieve_body($response), true);
            $code = (int) wp_remote_retrieve_response_code($response);
        }

        return $output;
    }
}
