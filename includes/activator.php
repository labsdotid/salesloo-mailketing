<?php

namespace Salesloo_Mailketing;

/**
 * Fired during plugin activation
 *
 * @link       https://www.fiqhidayat.com
 * @since      1.0.0
 *
 * @package    Salesloo_Mailketing
 * @subpackage Salesloo_Mailketing/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Salesloo_Mailketing
 * @subpackage Salesloo_Mailketing/includes
 * @author     Taufik Hidayat <taufik@fiqhidayat.com>
 */
class Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
	}
}
