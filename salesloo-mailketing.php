<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiqhidayat.com
 * @since             1.0.0
 * @package           Salesloo_Mailketing
 *
 * @wordpress-plugin
 * Plugin Name:       Salesloo Mailketing
 * Plugin URI:        https://www.salesloo.com
 * Description:       Mailketing autoresponder integration
 * Version:           1.0.3
 * Author:            Salesloo
 * Author URI:        https://www.salesloo.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       salesloo-mailketing
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('SALESLOO_MAILKETING_VERSION', '1.0.3');
define('SALESLOO_MAILKETING_URL', plugin_dir_url(__FILE__));
define('SALESLOO_MAILKETING_PATH', plugin_dir_path(__FILE__));
define('SALESLOO_MAILKETING_ROOT', __FILE__);

require 'update-checker/plugin-update-checker.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/activator.php
 */
function activate_salesloo_mailketing()
{
	require_once plugin_dir_path(__FILE__) . 'includes/activator.php';
	Salesloo_Mailketing\Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/deactivator.php
 */
function deactivate_salesloo_mailketing()
{
	require_once plugin_dir_path(__FILE__) . 'includes/deactivator.php';
	Salesloo_Mailketing\Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_salesloo_mailketing');
register_deactivation_hook(__FILE__, 'deactivate_salesloo_mailketing');


/**
 * Main salesloo Addon class
 */
class Salesloo_Mailketing
{
	/**
	 * Instance
	 */
	private static $_instance = null;

	/**
	 * run
	 *
	 * @return Salesloo_Tripay An instance of class
	 */
	public static function instance()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		add_action('plugins_loaded', [$this, 'on_plugins_loaded']);
	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @access public
	 */
	public function i18n()
	{
		load_plugin_textdomain(
			'salesloo-mailketing',
			false,
			dirname(plugin_basename(__FILE__)) . '/languages'
		);
	}

	/**
	 * On Plugins Loaded
	 *
	 * Checks if Salesloo has loaded, and performs some compatibility checks.
	 *
	 * @access public
	 */
	public function on_plugins_loaded()
	{

		if ($this->is_compatible()) {
			$this->i18n();
			$this->load_scripts();
			$this->init();
			$this->install_hooks();
		}
	}

	/**
	 * load_scripts
	 *
	 * @return void
	 */
	private function load_scripts()
	{
		require_once SALESLOO_MAILKETING_PATH . 'includes/setting.php';
		require_once SALESLOO_MAILKETING_PATH . 'includes/connect.php';
		require_once SALESLOO_MAILKETING_PATH . 'includes/event.php';
	}

	/**
	 * init salesloo tripay
	 *
	 * @return void
	 */
	private function init()
	{
		\Salesloo_Mailketing\Setting::init();
		\Salesloo_Mailketing\Event::init();
	}

	/**
	 * install hooks
	 *
	 * @return void
	 */
	private function install_hooks()
	{
	}

	/**
	 * Compatibility Checks
	 *
	 * @access public
	 */
	public function is_compatible()
	{
		// Check if Salesloo installed and activated
		if (!did_action('salesloo/loaded')) {
			add_action('admin_notices', [$this, 'admin_notice_missing_main_plugin']);
			return false;
		}

		if (version_compare(SALESLOO_VERSION, '1.0.5', '<')) {
			add_action('admin_notices', [$this, 'admin_notice_missing_main_plugin_version']);
			return false;
		}

		return true;
	}


	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Salesloo installed or activated.
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin()
	{

		if (isset($_GET['activate'])) unset($_GET['activate']);

		$message = sprintf(
			/* translators: 1: Salesloo Affiliate Bonus 2: Salesloo */
			esc_html__('"%1$s" requires "%2$s" to be installed and activated.', 'salesloo-tripay'),
			'<strong>' . esc_html__('Salesloo Tripay', 'salesloo-tripay') . '</strong>',
			'<strong>' . esc_html__('Salesloo', 'salesloo-tripay') . '</strong>'
		);

		printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Salesloo installed or activated.
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin_version()
	{

		if (isset($_GET['activate'])) unset($_GET['activate']);

		$message = sprintf(
			/* translators: 1: Salesloo Affiliate Bonus 2: Salesloo */
			esc_html__('"%1$s" requires "%2$s" version 1.0.5 or higher.', 'salesloo-tripay'),
			'<strong>' . esc_html__('Salesloo Tripay', 'salesloo-tripay') . '</strong>',
			'<strong>' . esc_html__('Salesloo', 'salesloo-tripay') . '</strong>'
		);

		printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
	}
}

Salesloo_Mailketing::instance();
